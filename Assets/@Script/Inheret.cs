﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Nah ini class yang mau nge-Inheret
//mantab kan
//trus yang sering dipake tu singleton
//itu design pattern jg
//KLo inheretence mononya dihapus soalnya dari class ParentInheretance, dia udh derive dri monobehaviour, biar ga double
public class Inheret : ParentInheretance
{
    public static Inheret Instance;
    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        Kijang();
        KijangAneh();
        //Cara panggil singleton
        // nama class + nama variabelnya yang di singleton + yang pengen kmu panggil
        Test_Singletonj.Instance.Kijang2();
    }

    // Update is called once per frame
    void Update()
    {
        //Test_Singletonj.Instance.Kijang2();
    }

    //ini ya?
    //Override tu kmu brati pengen mengganti atau menambahkan fungsi dari void ini, jadi fungsi turunan
    //iya virtual tu klo kmu mau polimorph
    //Polimorhpism
    public override void KijangAneh()
    {
        //Jadi di sini polimorph tu misal kmu punya karakter sama2 boxer tpi cara ninjunya beda
        //base kijangAneh tu isinya kesamaan boxer mereka
        //ini basenya
        base.KijangAneh();

        //ini represent beda tinjunya
        //gitu, Iyaap
        //ini tambahannya
        Debug.Log("Kijang 3 bareng kijang aneh");
    }

    //Poin ke 3?
}
