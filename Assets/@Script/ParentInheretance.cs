﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Ini calss yang bakal kita inheret
public class ParentInheretance : MonoBehaviour
{
    //sini?
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Klo mau pake inheretance kmu bisa pake overide method
    //maaf aku mikir nama method kadang lama
    public void Kijang()
    {
        Debug.Log("Kijang 1 lapor");
    }

    //ini contoh polimorph
    public virtual void KijangAneh()
    {
        Debug.Log("Kijang aneh");
    }
}
