﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Gini penulisan singleton paling sederhana
//Singleton ini biasanya dipake di gamemanager, UImanager, Setting, pokoknya semua class yang mungkin kmu butuh buat akses secara gampang dan berulang
public class Test_Singletonj : MonoBehaviour
{
    //Bedanya klo pake singleton, jangan sampe monobehavior ini ke destroy atau hilang atau ga keload
    //klo singleton kmu cuma nambah ini =v
    public static Test_Singletonj Instance;
    //yep harus public
    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Inheret.Instance.KijangAneh();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Kijang2()
    {
        Debug.Log("Kijang 2 lapor");
    }
}
